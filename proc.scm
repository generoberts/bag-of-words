;; (import (bag-of-words random))
(import (rnrs hashtables))
;; (load "random.scm")

;; The symbols we have to remove from the string
(define dets '(#\. #\, #\! #\?))

(define *db* (make-hashtable string-ci-hash string-ci=?))

(define (string-car str)
  (let ((the-str (if (char? str)
                     (string str)
                     str)))
    (if (> (string-length the-str) 0)
        (string-ref the-str 0)
        (error "Empty string doesn't have car."))))

(define (string-cdr str)
  (let ((the-str (if (char? str)
                     (string str)
                     str)))
    (if (> (string-length the-str) 1)
        (substring the-str 1 (string-length the-str))
        (error "String of length 1 doesn't have cdr." the-str))))

;; locate all the indexes of space in a given string
;; collector must given as '()
;; count must given as 0
(define (locate-spaces str collector count)
  (let ((the-str (if (char? str)
                     (string str)
                     str)))
    (cond
     ((equal? 1 (string-length str))
      (reverse collector))
     ((char-whitespace? (string-car str))
      (locate-spaces (string-cdr str) (cons count collector) (+ 1 count)))
     (else
      (locate-spaces (string-cdr str) collector (+ 1 count))))))

(define (remove-space-from-front str)
  (if (char-whitespace? (string-car str))
      (remove-space-from-front (string-cdr str))
      str))

(define (remove-space-from-back str)
  (let ((the-str (if (char? str)
                     (string str)
                     str)))
    (list->string
     (reverse
      (string->list
       (remove-space-from-front (list->string
                                 (reverse
                                  (string->list the-str)))))))))

(define (trim str)
  (remove-space-from-back
   (remove-space-from-front str)))

(define (string-split-2 str space-locations collector)
  (cond
   ((null? space-locations) (list str)) ; this string has no space
   ((= (length space-locations) 1)
    (cons (substring str 0 (car space-locations))
          (cons (substring str (car space-locations) (string-length))
                collector))) ; TODO test this case
   (else
    ;; TODO still draft at this else case
    (string-split-2 str (cdr space-locations) collector))))

(define (string-split str collector)
  (let ((space-locations (locate-spaces (trim str) '() 0))
        (first? #true)) ; all location of spaces in the string
    ;; start with 0, and each loop i will be whatever the first item of the remaining
    ;; of the space-locations
    (do ((i 0 (car space-locations)))
        ;; if there is no next item
        ((or (null? space-locations)
             (null? (cdr space-locations)))
         ;; cons the substring, starting with the last item of space-locations and
         ;; till the end of the string, with the collector
         (if first?
             str ;; this string has no spaces
             (cons (substring str (+ i 1) (length str)) collector)))
      (set! collector (cons (substring str
                                       (if first?
                                           i
                                           (+ i 1))
                                       (if first?
                                           (car space-locations)
                                           (cadr space-locations)))
                            collector))
      (set! space-locations (if first?
                                space-locations
                                (cdr space-locations)))
      (if first? (set! first? #false)))))

(define (remove-dets-front word)
  (if (member (string-car word) dets)
      (remove-dets-front (string-cdr word))
      word))

(define (remove-dets-back word)
  (let ((the-str (if (char? word)
                     (string word)
                     word)))
    (list->string
     (reverse
      (string->list
       (remove-dets-front
        (list->string
         (reverse
          (string->list the-str)))))))))

(define (remove-dets list-of-words)
  (map (lambda (word) (remove-dets-back (remove-dets-front word)))
       list-of-words))

(define (add-words-to-db list-of-words)
  (for-each (lambda (word)
                (if (hashtable-contains? *db* word)
                    (hashtable-set! *db* word (cons word (hashtable-ref *db* word 'NULL)))
                    (hashtable-set! *db* word (cons word '()))))
            list-of-words))

(define (add-string-to-db str)
  ;; (let ((list-of-words (remove-dets (string-split (string-downcase str) '()))))
    (let ((list-of-words (string-split (string-downcase str) '())))
      (add-words-to-db list-of-words)))

(define (add-to-db list-of-strings)
  (for-each (lambda (str)
              (add-string-to-db str))
            list-of-strings))

(trace add-words-to-db)
(trace add-string-to-db)
(trace add-to-db)
(trace string-split)
(trace locate-spaces)

(define lstr '("a quick" "brown fox" "jupms over the" "lazy dog." "a lazy fox" "sleep uner the tree???"))
(add-to-db lstr)
