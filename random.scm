(define-library (bag-of-word random)
  (export randomreal randomint)
  (import (scheme base)
          (scheme inexact))
  (begin
    (define randomreal
      (let ((a 48271) (c 0) (m (- (expt 2 31) 1)) (seed 16421225))
        (lambda new-seed
          (if (pair? new-seed)
              (set! seed (car new-seed))
              (set! seed (modulo (+ (* seed a) c) m)))
          (inexact (/ seed m)))))

    ;; 0 to limit - 1
    (define (randomint n)
      (modulo (exact (* (- (expt 2 31) 1) (randomreal))) n))))
