OBJ = proc.class random.class

%.class: %.scm
	kawa -C $<

main: $(OBJ)
	kawa --main -C main.scm

clear:
	rm -f *.class && rm -f ./bag-of-words/*.class

.PONY: clear
