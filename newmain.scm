;; rewritting

(require 'list-lib) ; kawa specific

;; this is where we store all the info
;; it's basically a lookup table
;; *db* is of the form
;; '((key1 value1-1 value1-2) (key2 value2-1 value2-2) ...)
(define *db* (list))

;; keeping track of keys in *db* so we don't have to seach for it everytime.
(define *keys* (list))

;; given a list of string, return a list with a space string " " between them
(define (%add-space-between-words . words)
  (let loop ((original-words words)
             (result '()))
    (cond
     ((null? original-words)
      (reverse result))
     ((odd? (length result)) ; not yet add space string to the result
      (loop original-words
            (cons " " result)))
     (else
      (loop (cdr original-words)
            (cons (car original-words) result))))))

;; concate each word with a space between them
(define (concate-words . words)
  (apply string-append (apply %add-space-between-words words)))

;; splitting a line of text into a list of words
(define (line->list line)
  (string-split line " "))

;; getting first n elements of a list
(define (get-first n lst)
  (let loop ((full-list lst)
             (result '()))
    (if (= n (length result))
        (reverse result)
        (loop (cdr full-list)
              (cons (car full-list) result)))))

;; like `substring'
(define (sublist lst start end)
  (let ((len (length lst)))
    (unless (and (>= start len)
                 (<= end len))
      (error "Invalid index" sublist))
    (do ((i start (+ i 1))
         (result '()))
        ((= i end) (reverse result))
      (set! result (cons (list-ref lst i) result)))))

;; for a list of N element, taking a list of first n element (start from index 0),
;; and a lits of the next n element (start from index 1) and so on, and return a list of those group.
(define (grouping-words n list-of-words)
  (let loop ((original-list list-of-words)
             (result '()))
    (if (< (length original-list) n)
        (reverse result)
        (loop (cdr original-list)
              (cons (get-first n original-list) result)))))

;; get a key-value list from *db*
(define (assoc-db key)
  (let loop ((all-key-value (list-copy *db*)))
    (cond
     ((null? all-key-value)
      #f)
     ((string=? (caar all-key-value) key)
      (car all-key-value))
     (else
      (loop (cdr all-key-value))))))

;; getting the corresponding value from a given key from *db*
(define (value-from-db key)
  (cdr (assoc-db key)))

;; a key is a string, value is also a string.
(define (add-to-db key value)
  (if (not (assoc-db key))
      (begin
        (set! *db* (if (null? *db*)
                       (list (append *db* (list key value)))
                       (append *db* (list (list key value)))))
        (set! *keys* (append *keys* (list key))))
      (let ((old-key-value (assoc-db key))
            (db-without-key (remove (lambda (x) (string=? (car x) key)) *db*)))
        (set! *db* (append db-without-key (list (append old-key-value (list value))))))))

;; for debugging
(define (%reset)
  (set! *db* (list))
  (set! *keys* (list)))
(define the-text "a b a b a a a b b a b a b b")

;; grouping a list of n words
(define (word-list->string-lists n word-list)
  (map (lambda (group)
         (apply concate-words group))
       (grouping-words n word-list)))

(define (text->db n text)
  (let* ((word-list (line->list text))
         (word-groups (word-list->string-lists n word-list))
         (counter n)) ; start at the first next word AFTER the given n word
    (for-each (lambda (word)
                (when (<= counter (length word-groups))
                  (add-to-db word (list-ref word-list counter))
                  (set! counter (+ counter 1))))
              word-groups)))

(define randomreal
      (let ((a 48271) (c 0) (m (- (expt 2 31) 1)) (seed 16421225))
        (lambda new-seed
          (if (pair? new-seed)
              (set! seed (car new-seed))
              (set! seed (modulo (+ (* seed a) c) m)))
          (inexact (/ seed m)))))

(define (randomint n)
      (modulo (exact (* (- (expt 2 31) 1) (randomreal))) n))

;; randomly pick an element from a list
(define (random-pick lst)
  (list-ref lst (randomint (length lst))))

;; generate total of LIMIT number of words
;; FIXME watch out about `reverse' and stuff like that
(define (ngram n limit text)
  (text->db n text)
  ;; since the `seed' is a string, and we want to look at its words, one-by-one to determine the first
  ;; `n` words of our result.
  (let* ((seed (random-pick *keys*))
         (seed-as-list (reverse (line->list seed)))
         (seed-length (length seed-as-list)))
    (let loop ((local-db (list-copy *db*))
               (result '()))
      (cond
       ((null? result)
        (loop local-db
              (append seed-as-list result)))
       ((= limit (length result))
        (reverse result)) ; FIXME
       (else
        (loop local-db
              (cons #;(random-pick
                     (value-from-db
                      (apply string-append
                       (reverse
                        (get-first n result)))))
                    (apply concate-words (reverse (get-first n result)))
                    result)))))))
