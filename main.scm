(load "random.scm")

(define (main)
  (do ((i 0 (+ 1 i)))
      ((= i 10))
    (display (random))
    (newline)))
